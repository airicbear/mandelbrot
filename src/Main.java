import java.util.ArrayList;

import processing.core.PApplet;

public class Main extends PApplet {
  double lowReal = -3;
  double highReal = 3;
  double realInterval = 0.1;
  double lowImag = -3;
  double highImag = 3;
  double imagInterval = 0.1;
  ArrayList<ComplexNumber> pointsOnScreen;

  public static void main(String[] args){
    main("Main");
  }

  public void settings(){
    size(600,600);
  }

  public float transformX(double x) {
    return (float) ((x - lowReal) * (width / (highReal - lowReal)));
  }

  public float transformY(double y) {
    return height - (float)((y - lowImag) * (height / (highImag - lowImag)));
  }

  public void drawSet() {
    background(255);
    MandelbrotSet m = new MandelbrotSet(lowReal, highReal, realInterval, lowImag, highImag, imagInterval);
    pointsOnScreen = new ArrayList<ComplexNumber>();
    for (ComplexNumber c : m.getSet()) {
      pointsOnScreen.add(c);
      println("Number of points: " + pointsOnScreen.size());
      point(transformX(c.getReal()), transformY(c.getImaginary()));
    }
  }

  public void increaseResolution() {
    realInterval*=0.5;imagInterval*=0.5;
    drawSet();
  }

  public void decreaseResolution() {
    realInterval*=2;imagInterval*=2;
    drawSet();
  }

  public void zoomIn(float x, float y) {
    double xShift = (highReal - lowReal) / 4.0;
    double yShift = (highImag - lowImag) / 4.0;
    lowReal = x - xShift;
    highReal = x + xShift;
    realInterval /= 2.0;
    lowImag = y - yShift;
    highImag = y + yShift;
    imagInterval /= 2.0;
    drawSet();
    if (pointsOnScreen.size() < 10) {
      zoomOut(x,y);
    }
  }

  public void zoomOut(float x, float y) {
    double xShift = (highReal - lowReal);
    double yShift = (highImag - lowReal);
    lowReal = x - xShift;
    highReal = x + xShift;
    realInterval *= 2.0;
    lowImag = y - yShift;
    highImag = y + yShift;
    imagInterval *= 2.0;
    drawSet();
    if (pointsOnScreen.size() < 10) {
      zoomIn(x,y);
    }
  }

  public void panDown(){
    lowImag-=imagInterval;
    highImag-=imagInterval;
    drawSet();
    if (pointsOnScreen.size() < 2) {
      panUp();
    }
  }

  public void panUp() {
    lowImag+=imagInterval;
    highImag+=imagInterval;
    drawSet();
    if (pointsOnScreen.size() < 2) {
      panDown();
    }
  }

  public void panRight() {
    lowReal+=realInterval;
    highReal+=realInterval;
    drawSet();
    if (pointsOnScreen.size() < 2) {
      panLeft();
    }
  }

  public void panLeft() {
    lowReal-=realInterval;
    highReal-=realInterval;
    drawSet();
    if (pointsOnScreen.size() < 2) {
      panRight();
    }
  }
  
  public void keyPressed() {
    switch (key) {
      case 'z':
        if (pointsOnScreen.size() < 200000) {
          increaseResolution();
        }
        break;
      case 'x':
        decreaseResolution();
        break;
      case 'w':
        zoomIn(zoomPointX(width / 2), zoomPointY(height / 2));
        break;
      case 's':
        zoomOut(zoomPointX(width/2), zoomPointY(height/2));
        break;
      case 'q':
        exit();
        break;
      case 'u':
        stroke(0,0,0);
        drawSet();
        break;
      case 'i':
        stroke(255,0,0);
        drawSet();
        break;
      case 'o':
        stroke(0,255,0);
        drawSet();
        break;
      case 'p':
        stroke(0,0,255);
        drawSet();
        break;
      case CODED:
        switch (keyCode) {
          case UP:
            panUp();
            break;
          case DOWN:
            panDown();
            break;
          case LEFT:
            panLeft();
            break;
          case RIGHT:
            panRight();
            break;
          default:
            break;
        }
      default:
        break;
    }
  }

  public float zoomPointX(float num) {
    return (float)((num/(float)width)*(highReal-lowReal)+lowReal);
  }

  public float zoomPointY(float num) {
    return (float)((height-num)/(float)height*(highImag-lowImag)+lowImag);
  }

  public void mouseClicked() {
    zoomIn(zoomPointX(mouseX), zoomPointY(mouseY));
  }

  public void setup() {
    background(255);
    drawSet();
  }

  public void draw() {
	  
  }
}
